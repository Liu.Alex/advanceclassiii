using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateApple : MonoBehaviour
{

    public GameObject apple;

    public Transform appleClass;

    Vector3 position1 = new Vector3(-1.039978f, -0.330002f, 0.0f);
    Vector3 position2 = new Vector3(-0.039978f, -1.330002f, 0.0f);
    Vector3 position3 = new Vector3(-0.039978f, -0.330002f, 0.0f);

    Quaternion rotation1 = new Quaternion(0, 0, 0, 0);

    void Start()
    {
        Instantiate(apple, position1, rotation1, appleClass);
        Instantiate(apple, position2, new Quaternion(0, 0, 0, 0), appleClass);
        Instantiate(apple, position3, Quaternion.identity, appleClass);

    }








}
