using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LoadFileTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            // string sPath = Application.dataPath;
            //  string sPath = Application.streamingAssetsPath;
            string sPath = Application.persistentDataPath;
            Debug.Log(sPath);
            SaveTxtfile(sPath + "/DEF.txt", "1234567 fdjshf");
            //LoadTxtfile(sPath + "/ABC.txt");
        }
    }

    public void LoadTxtfile(string filename) { 
        if(File.Exists(filename) == false)
        {
            return;
        }

        // method 1
        string strs = File.ReadAllText(filename);
        Debug.Log(strs);

        // method 2
        FileStream fs = File.Open(filename, FileMode.OpenOrCreate);
        StreamReader sr = new StreamReader(fs);
        string sLine = sr.ReadLine();
        Debug.Log(sLine);
        sr.Close();
        fs.Close();
    }

    public void SaveTxtfile(string filename, string sAll)
    {
        // method 1
       // File.WriteAllText(filename, sAll);

        // method 2
        FileStream fs = File.Open(filename, FileMode.OpenOrCreate);
        StreamWriter sw = new StreamWriter(fs);
        sw.Write(sAll);
        sw.Close();
        fs.Close();
    }
}
